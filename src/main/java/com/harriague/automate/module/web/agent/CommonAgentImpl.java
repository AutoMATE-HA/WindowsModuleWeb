package com.harriague.automate.module.web.agent;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.harriague.automate.core.agent.Agent;
import com.harriague.automate.core.conf.PropertiesKeys;
import com.harriague.automate.core.exceptions.AgentException;
import com.harriague.automate.core.exceptions.PropertyException;
import com.harriague.automate.core.structures.FlawedTimeUnit;
import com.harriague.automate.core.structures.ScrollDirection;
import com.harriague.automate.core.structures.SwipeDirection;
import com.harriague.automate.core.utils.ReadProperty;

public class CommonAgentImpl implements Agent {

    /**
     * Logger object
     */
    private static Logger log = Logger.getLogger(CommonAgentImpl.class.getName());

    /**
     * Browser Driver
     */
    private WebDriver driver;

    /**
     * is quick silver
     */
    private boolean searchingQuick = false;

    private WebElement lastElementUsed;

    private final FlawedTimeUnit DEFAULT_DRIVER_QUICK_SEARCH;

    private final String ATTRIBUTE_VALUE;

    private final String BROWSER_FIREFOX_NAME;

    private final String BROWSER_CHROME_NAME;

    private final String BROWSER_DEFAULT_NAME;

    private final String BROWSER_OPERA_NAME;

    private final String RESOURCES_FOLDER;

    private final String PATH_WEB_DRIVERS_FOLDER;

    private final String DRIVER_CHROME_FILE_NAME;

    private final String PROPERTY_CHROMEDRIVER;

    private final String DRIVER_OPERA_FILE_NAME;

    private final String DRIVER_DEFAULT_BROWSER_FILE_NAME;

    private final String PROPERTY_DEFAULT_BROWSER_DRIVER;

    protected CommonAgentImpl(FlawedTimeUnit default_driver_quick_search, String attribute_value,
            String fire_fox_name, String chrome_name, String defaul_browser_name, String opera_name,
            String resource_folder, String path_web_driver_folder, String driver_chrome_file_name,
            String property_chromeDriver, String driver_opera_file_name,
            String driver_default_browser_file_name, String property_default) {
        DEFAULT_DRIVER_QUICK_SEARCH = default_driver_quick_search;
        ATTRIBUTE_VALUE = attribute_value;
        BROWSER_FIREFOX_NAME = fire_fox_name;
        BROWSER_CHROME_NAME = chrome_name;
        BROWSER_DEFAULT_NAME = defaul_browser_name;
        BROWSER_OPERA_NAME = opera_name;
        RESOURCES_FOLDER = resource_folder;
        PATH_WEB_DRIVERS_FOLDER = path_web_driver_folder;
        DRIVER_CHROME_FILE_NAME = driver_chrome_file_name;
        PROPERTY_CHROMEDRIVER = property_chromeDriver;
        DRIVER_OPERA_FILE_NAME = driver_opera_file_name;
        DRIVER_DEFAULT_BROWSER_FILE_NAME = driver_default_browser_file_name;
        PROPERTY_DEFAULT_BROWSER_DRIVER = property_default;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.harriague.automate.core.agent.Agent#start(java.lang.String)
     */
    public void start(String applicationName) throws AgentException {
        String[] parameters = applicationName.split("-");
        if (parameters.length > 1) {
            openBrowser(parameters[1]);
        } else {
            openDefaultBrowser();
        }
        try {
            driver.navigate().to(parameters[0]);
        } catch (TimeoutException e) {
            driver.close();
            throw new AgentException("\nTHE BROWSER " + parameters[1] + " WAS BLOCKED:\n", e,this);
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.harriague.automate.core.agent.Agent#waitForVanish(java.lang.Object)
     */
    public void waitForVanish(Object element) throws AgentException {
        vanish(element, DEFAULT_DRIVER_QUICK_SEARCH);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.harriague.automate.core.agent.Agent#waitForVanish(java.lang.Object, int)
     */
    public void waitForVanish(Object element, FlawedTimeUnit timeout) throws AgentException {
        vanish(element, timeout);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.harriague.automate.core.agent.Agent#writeInElement(java.lang.Object,
     * java.lang.String)
     */
    public void writeInElement(Object element, String text) throws AgentException {

        WebElement webElement = quickFindElement((By) element, DEFAULT_DRIVER_QUICK_SEARCH);
        // webElement.click();
        webElement.sendKeys(text);
        if (!webElement.getAttribute(ATTRIBUTE_VALUE).equals(text)) {
            webElement.clear();
            webElement.sendKeys(text);
        }
    }

    @Override
    public void takeScreenshot() throws AgentException {
        File srcFiler = getScreenshotAs(OutputType.FILE);
        DateFormat dateFormat = new SimpleDateFormat("MM_dd_HH_mm_ss");
        String folder = com.harriague.automate.core.conf.Constants.SCREENSHOT_FOLDER_PATH;
        String fileName = "agentScreenshot_" + dateFormat.format(new Date().getTime());
        try {
            FileUtils.copyFile(srcFiler, new File(folder + "/" + fileName + ".png"));
        } catch (Exception e) {
        }
    }

    @Override
    public void highlightElement(Object element) throws AgentException {
        WebElement x = (WebElement) findElement((By) element);
        ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", x);
        WebDriverWait wait = new WebDriverWait(driver, 2);
        try {
            wait.until(ExpectedConditions.invisibilityOfElementLocated((By) element));
        } catch (TimeoutException e) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='0'", x);
        }
    }

    @Override
    public boolean checkElementIsDisplayed(Object element) throws AgentException {
        return quickFindElement((By) element, DEFAULT_DRIVER_QUICK_SEARCH) != null;
    }

    @Override
    public boolean checkElementIsDisplayed(Object element, FlawedTimeUnit timeout) {
        try {
            return quickFindElement((By) element, timeout) != null;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void pressKey(Object key) throws AgentException {
        log.info(lastElementUsed);
        /*
         * Actions builder = new Actions(driver); try{ builder.keyDown((Keys) key).perform();
         * }catch(Exception e){
         * 
         * // lastElementUsed.sendKeys((CharSequence[]) key); }
         */
    }

    @Override
    public void click(Object element) throws AgentException {
        WebElement el = quickFindElement((By) element, DEFAULT_DRIVER_QUICK_SEARCH);
        if (el == null)
            throw new AgentException("Element \"" + element + "\" was not found", this);
        else
            el.click();
    }

    @Override
    public void rightClick(Object by) throws AgentException {
        WebElement context = find((By) by);
        Actions action = new Actions(driver);
        action.contextClick(context).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ARROW_DOWN)
                .sendKeys(Keys.RETURN).build().perform();
    }

    @Override
    public void scroll(ScrollDirection direction, int amount) throws AgentException {
        if (direction.equals(ScrollDirection.UP))
            amount *= -1;
        ((JavascriptExecutor) driver).executeScript("window.scrollBy(0," + amount + ")", "");
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
        }
    }



    /**
     * Get default browser name
     * 
     * @throws AgentException
     */
    private void openDefaultBrowser() throws AgentException {
        openBrowser(ReadProperty.getProperty(PropertiesKeys.DEFAULT_BROWSER, BROWSER_FIREFOX_NAME));
    }

    /**
     * Open a browser by name
     * 
     * @param browserName browser name
     */
    private void openBrowser(String browserName) throws AgentException {
        final String BROWSER_NAME = browserName.toUpperCase();
        if (BROWSER_NAME.equals(BROWSER_CHROME_NAME)) {
            log.info("Open Chrome browser.");
            try {
                startChrome();
            } catch (IOException e) {
                throw new AgentException(e, null);
            }
        } else if (BROWSER_NAME.equals(BROWSER_FIREFOX_NAME)) {
            log.info("Open FireFox browser.");
            try {
                startFireFox();
            } catch (IOException e) {
                throw new AgentException(e, null);
            }
        } else if (BROWSER_NAME.equals(BROWSER_DEFAULT_NAME)) {
            log.info("Open Internet Explorer browser.");
            startInternetExplorer();
        } else if (BROWSER_NAME.equals(BROWSER_OPERA_NAME)) {
            log.info("Open Opera browser.");
            startOpera();
        } else {
            log.info("The browser '" + browserName + "' is not supported.");
        }
    }

    /**
     * Start Chrome browser
     */
    public void startChrome() throws IOException {
        driver = DriverBinaryManager.getChromeDriver();
    }

    public void startInternetExplorer() {

        File srcFile = new File(
                RESOURCES_FOLDER + PATH_WEB_DRIVERS_FOLDER + DRIVER_DEFAULT_BROWSER_FILE_NAME);
        System.setProperty(PROPERTY_DEFAULT_BROWSER_DRIVER, srcFile.getAbsolutePath());

        DesiredCapabilities caps = DesiredCapabilities.internetExplorer();
        caps.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
        caps.setCapability(InternetExplorerDriver.IE_SWITCHES, "-private");
        // HKLM_CURRENT_USER\\Software\\Microsoft\\Internet Explorer\\Main path should contain key
        // TabProcGrowth with 0 value.
        driver = new InternetExplorerDriver(caps);
        driver.manage().window().maximize();
    }

    public void startFireFox() throws IOException {
        driver = DriverBinaryManager.getFirefoxDriver();
        driver.manage().window().maximize();
    }

    public void startOpera() {
        File srcFile =
                new File(RESOURCES_FOLDER + PATH_WEB_DRIVERS_FOLDER + DRIVER_OPERA_FILE_NAME);
        System.setProperty(PROPERTY_CHROMEDRIVER, srcFile.getAbsolutePath());
        driver = new ChromeDriver();
    }

    private WebElement findInAllIframe(By by) {
        WebElement element = null;
        Set<String> windows = driver.getWindowHandles();
        for (int i = (windows.size() - 1); i >= 0; i++) {
            driver.switchTo().window((String) windows.toArray()[i]);
            driver.switchTo().defaultContent();
            element = find(by);
            if (element != null) {
                break;
            }
        }
        return element;
    }

    @SuppressWarnings("unchecked")
    private ArrayList<WebElement> findAllInAllIframe(By by) {
        ArrayList<WebElement> element = null;
        Set<String> windows = driver.getWindowHandles();
        for (int i = (windows.size() - 1); i >= 0; i++) {
            driver.switchTo().window((String) windows.toArray()[i]);
            driver.switchTo().defaultContent();
            element = (ArrayList<WebElement>) find(by, null, true);
            if (element != null) {
                break;
            }
        }
        return element;
    }

    private WebElement find(By by) {
        return (WebElement) find(by, null, false);
    }


    private Object find(By by, List<WebElement> iframe, boolean all) {
        Object element = null;
        if (iframe == null) {
            driver.switchTo().defaultContent();
            try {
                if (all)
                    element = driver.findElements(by);
                else
                    element = driver.findElement(by);
            } catch (Exception exception) {
            }
            if (element == null) {
                List<WebElement> frameRoot = new ArrayList<WebElement>();
                List<WebElement> iframes = new ArrayList<WebElement>();
                iframes.addAll(driver.findElements(By.tagName("iframe")));
                iframes.addAll(driver.findElements(By.tagName("frame")));
                if (iframes != null && !iframes.isEmpty()) {
                    for (WebElement iframeId : iframes) {
                        frameRoot.add(iframeId);
                        element = find(by, frameRoot, all);
                        frameRoot.remove(iframeId);
                        if (element != null) {
                            break;
                        }
                    }
                }
            }
        } else {
            driver.switchTo().defaultContent();
            for (WebElement i : iframe) {
                driver.switchTo().frame(i);
            }

            try {
                if (all)
                    element = driver.findElements(by);
                else
                    element = driver.findElement(by);
            } catch (Exception exception) {
            }

            if (element == null) {
                List<WebElement> iframes = new ArrayList<WebElement>();
                iframes.addAll(driver.findElements(By.tagName("iframe")));
                iframes.addAll(driver.findElements(By.tagName("frame")));
                if (iframes != null && !iframes.isEmpty())
                    for (WebElement iframeId : iframes) {
                        iframe.add(iframeId);
                        element = find(by, iframe, all);
                        iframe.remove(iframeId);
                        if (element != null) {
                            break;
                        }
                    }
            }
        }
        if (!all && element != null)
            lastElementUsed = (WebElement) element;
        return element;
    }



    // private WebElement find2(By by) {
    // List<WebElement> iframes = driver.findElements(By.tagName("iframe"));
    // List<WebElement> iframesQueue = new ArrayList<WebElement>();
    // WebElement element = null;
    // if( !iframes.isEmpty() ) {
    // for (WebElement iframe : iframes) {
    // driver.switchTo().defaultContent();
    // WebDriverWait wait = new WebDriverWait(driver,10);
    // try {
    // wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframe.getAttribute("id")));
    // } catch (TimeoutException e1) {
    // wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframe.getAttribute("name")));
    // }
    // try {
    //// element = wait.until(ExpectedConditions.elementToBeClickable(by));
    // element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    // } catch (Exception e) {
    // iframesQueue.add(iframe);
    // element = nestedIframeSearch(by, iframesQueue);
    // }
    // if(element != null) {
    // return element;
    // }
    // }
    // }
    // if(element == null) {
    // driver.switchTo().defaultContent();
    //// element = driver.findElement(by);
    // WebDriverWait wait = new WebDriverWait(driver,10);
    // element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    // }
    // return element;
    // }
    //
    // private WebElement nestedIframeSearch(By by, List<WebElement> parents){
    // List<WebElement> iframes = driver.findElements(By.tagName("iframe"));
    // iframes.addAll(driver.findElements(By.tagName("frame")));
    // WebElement element = null;
    // if( !iframes.isEmpty() ) {
    // for (WebElement iframe : iframes) {
    // driver.switchTo().defaultContent();
    // for (WebElement auxiliar : parents) {
    // WebDriverWait wait = new WebDriverWait(driver,10);
    // try {
    // wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(auxiliar.getAttribute("id")));
    // } catch (TimeoutException e) {
    // wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframe.getAttribute("name")));
    // }
    // }
    // WebDriverWait wait = new WebDriverWait(driver,10);
    // try {
    // wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframe.getAttribute("id")));
    // } catch (TimeoutException e1) {
    // wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframe.getAttribute("name")));
    // }
    // try {
    //// element = wait.until(ExpectedConditions.elementToBeClickable(by));
    // element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    // } catch (Exception e) {
    // parents.add(iframe);
    // element = nestedIframeSearch(by, parents);
    // }
    // if(element != null) {
    // return element;
    // }
    // }
    // }
    // //if iframe is empty, we the remove last parent for next iteration
    // parents.remove((parents.size()-1));
    // return null;
    // }

    /**
     * Search an element for the timeout specified
     * 
     * @param by to element
     * @param timeout search time out
     * @return WebElement
     * @throws AgentException
     */
    public WebElement quickFindElement(By by, FlawedTimeUnit timeout) throws AgentException {
        searchingQuick = true;
        WebElement element = null;
        FlawedTimeUnit clock = new FlawedTimeUnit();
        final FlawedTimeUnit timeBetweenSearches = FlawedTimeUnit.seconds(1);
        while(clock.isLesserThan(timeout)) {
            clock.waitFor(timeBetweenSearches);
            element = (WebElement) findElement(by);
            if (element != null) {
                lastElementUsed = element;
                break;
            }
        }
        searchingQuick = false;
        return element;
    }

    /**
     * Wait a vanish an element
     * 
     * @param element to vanish
     * @param timeOut
     * @throws AgentException
     */
    private void vanish(Object element, FlawedTimeUnit timeOut) throws AgentException {
        By by = (By) element;
        boolean vanished = false;
        int attempts = 10;
        int currentAttempt = 0;

        while (!vanished && currentAttempt < attempts) {
            vanished = (quickFindElement(by, timeOut) == null);
            currentAttempt++;
        }
        log.info("Element vanished: " + vanished);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.harriague.automate.core.agent.Agent#close()
     */
    @Override
    public void close() {
        driver.quit();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.harriague.automate.core.agent.Agent#navegateTo(java.lang.String)
     */
    public void navigateTo(String url) throws AgentException {
        try {
            driver.navigate().to(url);
        } catch (TimeoutException e) {
            driver.close();
            throw new AgentException("\nTHE BROWSER WAS BLOCKED:\n", e, this);
        }
    }

    @Override
    public String getTextValue(Object element) throws AgentException {
        return find((By) element).getText();
    }

    @Override
    public String getValue(Object element, String attribute) throws AgentException {
        return ((WebElement) element).getAttribute(attribute);
    }

    @Override
    public List<Map<String, Object>> getGrid(Object id) throws AgentException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void selectInCombobox(Object xpath, Object value) throws AgentException {
        boolean isSelected = false;
        Select select = new Select(quickFindElement((By) xpath, DEFAULT_DRIVER_QUICK_SEARCH));
        for (int i = 0; i < select.getOptions().size(); i++) {
            if (select.getOptions().get(i).getText().contains((String) value)) {
                select.selectByIndex(i);
                isSelected = true;
                break;
            }
        }
        if (isSelected) {
            log.info("The option '" + ((String) value) + "' is selected.");
        } else {
            throw new AgentException("The option '" + ((String) value) + "' not exist", this);
        }
    }

    @Override
    public void maximizeWindows() throws AgentException {
        ((JavascriptExecutor) driver)
                .executeScript("window.moveTo(0,0); window.resizeTo(screen.width, screen.height);");
    }

    @Override
    public Object hover(Object xpath) throws AgentException {
        Actions action = new Actions(driver);
        WebElement we = findInAllIframe((By) xpath);
        action.moveToElement(we).perform();
        return null;
    }

    @Override
    public void scrollIntoView(Object element) throws AgentException {
        WebElement webElement = find((By) element);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",
                webElement);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object findElement(Object by) throws AgentException {
        By element = (By) by;
        WebElement res = null;
        try {
            res = findInAllIframe(element);
        } catch (Exception e) {
            if (searchingQuick) {
                log.info("Element not displayed:  " + by.toString());
            } else {
                log.info(driver.getPageSource());
                throw e;
            }
        }
        return res;
    }

    @Override
    public void back() throws AgentException {
        driver.navigate().back();
    }

    @Override
    public Object findElements(Object by) {
        return findAllInAllIframe((By) by);
    }

    @Override
    public void longPressKey(Object key) {
        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL).sendKeys((CharSequence[]) key).perform();
    }

    @Override
    public <X> X getScreenshotAs(OutputType<X> outputType) throws WebDriverException {
        return ((TakesScreenshot) driver).getScreenshotAs(outputType);
    }

    @Override
    public int getHeight() {
        return Integer.parseInt(((JavascriptExecutor) driver).executeScript("Math.max(document.documentElement.clientHeight, window.innerHeight || 0)").toString());
    }

    @Override
    public int getWidth() {
        return Integer.parseInt(((JavascriptExecutor) driver).executeScript("Math.max(document.documentElement.clientWidth, window.innerWidth || 0)").toString());
    }

    @Override
    public BufferedImage takeScreenshotAsBuffer() throws AgentException{
        log.info("taking screenShot");
        File scrFile = getScreenshotAs(OutputType.FILE);
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(scrFile);
        } catch (IOException e) {
            throw new AgentException(e, this);
        }
        return bufferedImage;
    }

    @Override
    public void clickInTheScreen(int fingers, int positionX, int positionY, FlawedTimeUnit duration) throws AgentException{
        // TODO find a way of doing this
        throw new AgentException("doing this is bad practice", this);
    }

    /**
     * WebAgent Methods
     */

    /**
     * waits until condition is met or the driver times out
     * @param condition the condition that must be met
     */
    public void waitUntil(ExpectedCondition<WebElement> condition) {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(condition);
    }

    @Override
    public Process sendCommand(String command) throws AgentException {
        try {
            return Runtime.getRuntime().exec(command);
        } catch (IOException e) {
            throw new AgentException(e,this);
        }
    }

    @Override
    public Process waitForCommand(String command) throws AgentException {
        Process p = sendCommand(command);
        try {
            p.waitFor();
        } catch (InterruptedException e) {
            throw new AgentException(e, this);
        }
        return p;
    }

    @Override
    public void swipe(SwipeDirection direction) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void swipe(int startx, int starty, int endx, int endy, FlawedTimeUnit duration) {
        // TODO Auto-generated method stub
        
    }

}
