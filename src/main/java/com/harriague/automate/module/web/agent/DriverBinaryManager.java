package com.harriague.automate.module.web.agent;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.harriague.automate.core.utils.CoreUtils;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DriverBinaryManager {

    static FirefoxDriver getFirefoxDriver() throws IOException {
        StringBuilder path = new StringBuilder(35);//    resources/ff/win/64/geckodriver.exe
        StringBuilder xpath = new StringBuilder(37);//   //*[contains(@href,'linux64.tar.gz')]
        StringBuilder target = new StringBuilder(25);//  target/tmp/linux64.tar.gz
        String endFile = "";
        xpath.append("//*[contains(@href,'");
        target.append("target/tmp/");
        path.append("resources");
        path.append(File.separatorChar);
        path.append("ff");
        path.append(File.separatorChar);
        if(SystemUtils.IS_OS_WINDOWS) {
            path.append("win");
            xpath.append("win");
            target.append("win64.zip");
            endFile = ".zip";
        } else if(SystemUtils.IS_OS_LINUX) {
            path.append("linux");
            xpath.append("linux");
            target.append("linux64.tar.gz");
            endFile = ".tar.gz";
        } else if(SystemUtils.IS_OS_MAC) {
            target.append("macos.tar.gz");
            path.append("mac");
        }
        path.append(File.separatorChar);
        if(CoreUtils.is64BitOS()) {
            path.append("64");
            xpath.append("64");
        } else {
            path.append("32");
            xpath.append("32");
        }
        xpath.append(endFile);
        xpath.append("')]");
        path.append(File.separatorChar);
        path.append("geckodriver");
        if(SystemUtils.IS_OS_WINDOWS) {
            path.append(".exe");
        }
        String finalPath = path.toString();
        File pathToBinary = new File(finalPath);
        if(!pathToBinary.exists()) {
            File folder = pathToBinary.getParentFile();
            folder.mkdirs();
            HtmlUnitDriver driver = new HtmlUnitDriver(false);
            driver.get("https://github.com/mozilla/geckodriver/releases");
            WebElement el = driver.findElement(By.xpath(xpath.toString()));
            File zipFile = new File(target.toString());
            downloadFile(el.getAttribute("href"), zipFile);
            if(zipFile.getPath().endsWith(".zip")) {
                unZip(zipFile, folder);
            } else {
                unTar(zipFile, folder);
            }
        }
        System.setProperty("webdriver.gecko.driver",finalPath);
        String binPath = System.getenv("webdriver.firefox.bin");
        if(binPath != null) {
            FirefoxOptions opt = new FirefoxOptions();
            opt.setBinary(binPath);
            return new FirefoxDriver(opt);
        }
        return new FirefoxDriver();
    }

    public static ChromeDriver getChromeDriver() throws IOException {
        StringBuilder path = new StringBuilder(34);//              resources/chr/win/chromedriver.exe
        StringBuilder xpath = new StringBuilder(28);//             //*[contains(@href,'linux')]
        StringBuilder target = new StringBuilder(19);//            target/tmp/chrl.zip
        StringBuilder downloadLocation = new StringBuilder(73);//  https://chromedriver.storage.googleapis.com/2.40/chromedriver_linux64.zip
        path.append("resources");
        path.append(File.separatorChar);
        path.append("chr");
        path.append(File.separatorChar);
        xpath.append("//*[contains(@href,'");
        target.append("target/tmp/chr");
        downloadLocation.append("https://chromedriver.storage.googleapis.com/");
        if(SystemUtils.IS_OS_WINDOWS) {
            path.append("win");
            xpath.append("win");
            target.append('w');
        } else if(SystemUtils.IS_OS_LINUX) {
            path.append("linux");
            xpath.append("linux");
            target.append('l');
        } else if(SystemUtils.IS_OS_MAC) {
            path.append("mac");
            xpath.append("mac");
            target.append('m');
        }
        xpath.append("')]");
        path.append(File.separatorChar);
        path.append("chromedriver");
        target.append(".zip");
        if(SystemUtils.IS_OS_WINDOWS) {
            path.append(".exe");
        } else if(SystemUtils.IS_OS_LINUX) {
            path.append("linux");
        } else if(SystemUtils.IS_OS_MAC) {
            path.append("mac");
        }
        String finalPath = path.toString();
        File pathToBinary = new File(finalPath);
        if(!pathToBinary.exists()) {
            File folder = pathToBinary.getParentFile();
            folder.mkdirs();
            HtmlUnitDriver driver = new HtmlUnitDriver(true);
            driver.get("http://chromedriver.chromium.org/downloads");
            WebElement el = driver.findElement(By.xpath("//*[contains(@href,'chromedriver.storage.googleapis.com/index')]"));
            String href = el.getAttribute("href");
            int e = href.indexOf('/',60);
            downloadLocation.append(href.substring(60,e));
            if(SystemUtils.IS_OS_WINDOWS) {
                downloadLocation.append("/chromedriver_win32.zip");
            } else if(SystemUtils.IS_OS_LINUX) {
                downloadLocation.append("/chromedriver_linux64.zip");
            } else if(SystemUtils.IS_OS_MAC) {
                downloadLocation.append("/chromedriver_mac64.zip");
            }
            File zipFile = new File(target.toString());
            downloadFile(downloadLocation.toString(), zipFile);
            unZip(zipFile, folder);
        }
        System.setProperty("webdriver.chrome.driver", pathToBinary.getAbsolutePath());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        return new ChromeDriver(options);
    }

    private static void downloadFile(String url, File downloadPath) throws IOException {
        URL website = new URL(url);
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        downloadPath.getParentFile().mkdirs();
        FileOutputStream fos = new FileOutputStream(downloadPath);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
    }

    private static void unZip(File zipPath, File outputFolder) throws IOException {
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(zipPath));
        ZipEntry zipEntry = zis.getNextEntry();
        while(zipEntry != null){
            FileOutputStream fos2 = new FileOutputStream(outputFolder.getPath()+File.separator+zipEntry.getName());
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos2.write(buffer, 0, len);
            }
            fos2.close();
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
    }

    private static void unTar(File zipPath, File outputFolder) throws IOException {
        InputStream fi = Files.newInputStream(zipPath.toPath());
        InputStream bi = new BufferedInputStream(fi);
        InputStream gzi = new GzipCompressorInputStream(bi);
        TarArchiveInputStream i = new TarArchiveInputStream(gzi);
        TarArchiveEntry tar = null;
        while((tar = i.getNextTarEntry()) != null) {
            if(tar.isDirectory()) {
                new File(outputFolder.getPath()+tar.getName()).mkdirs();
            } else {
                IOUtils.copy(i, Files.newOutputStream(FileSystems.getDefault().getPath(outputFolder.getPath()+tar.getName())));
            }
        }
    }
}
